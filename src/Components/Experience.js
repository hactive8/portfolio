import React, { Component } from 'react'

export default class Experience extends Component {
    constructor() {
        super()
    
        this.state = {
            experiences: [
                {
                    title: "IT Development & Infrastruktur Manager",
                    company: "PT Bhanda Ghara Reksa (Persero)",
                    description: "Responsible for providing infrastructure, application and network services and Collaborating within the IT division in developing various applications and systems",
                    period: "April 2018 - Present"
                },
                {
                    title: "IT Supervisor",
                    company: "PT Bhanda Ghara Reksa (Persero) / BGR Express",
                    description: "Responsible for providing infrastructure, application and network services for BGR Express Unit",
                    period: "January 2016 - April 2018"
                },
                {
                    title: "IT Supervisor",
                    company: "PT Yapindo Transportama",
                    description: "Responsible for providing infrastructure, application and network services",
                    period: "January 2010 - Desember 2015"
                },
                
            ]
        }
    }

    render() {
        var experiences = this.state.experiences.map(experience => {
            return (
                <div key={experience.title} className="resume-item d-flex flex-column flex-md-row justify-content-between mb-5">
                    <div className="resume-content">
                        <h3 className="mb-0">{experience.title}</h3>
                        <div className="subheading mb-3">{experience.company}</div>
                        <p>{experience.description}</p>
                    </div>
                    <div className="resume-date text-md-right">
                        <span className="text-primary">{experience.period}</span>
                    </div>
                </div>
            )
        })

        return (
            <section className="resume-section p-3 p-lg-5 d-flex justify-content-center" id="experience">
                <div className="w-100">
                    <h2 className="mb-5">Experience</h2>
                    {experiences}                    
                </div>
            </section>
        )
    }
}
