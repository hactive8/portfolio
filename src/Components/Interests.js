import React, { Component } from 'react'

export default class Interests extends Component {
    render() {
        return (
            <section className="resume-section p-3 p-lg-5 d-flex align-items-center" id="interests">
                <div className="w-100">
                    <h2 className="mb-5">Interests</h2>
                    <p className="mb-0">I like to spend time for sports such as soccer and badminton.</p>
                </div>
            </section>
        )
    }
}
