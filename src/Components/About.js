import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faLinkedinIn, faGithub, faTwitter, faFacebookF, faInstagram } from '@fortawesome/free-brands-svg-icons'

export default class About extends Component {
    render() {
        return (
            <section className="resume-section p-3 p-lg-5 d-flex align-items-center" id="about">
                <div className="w-100">
                    <h2 className="mb-0">Ahmad <span className="text-primary">Sobirin S.Kom</span></h2>
                    {/* <div className="subheading mb-5">3542 Berry Street · Cheyenne Wells, CO 80810 · (317) 585-8468 · <a href="mailto:name@email.com">name@email.com</a> */}
                    <div className="subheading mb-5">
                        Tangerang, Indonesia · <a href="tel:+628119609600">+628119609600</a> · <a href="mailto:birin243@gmail.com">birin243@gmail.com</a>
                    </div>
                    <p className="lead mb-5">
                        Manager IT Development & Infrastruktur at PT Bhanda Ghara Reksa (Persero).
                    </p>
                    <div className="social-icons">
                        {/* <a href="https://github.com/e-hastono" target="_blank" rel="noopener noreferrer">
                            <FontAwesomeIcon icon={faGithub} />
                        </a> */}
                        <a href="https://twitter.com/birin_243" target="_blank" rel="noopener noreferrer">
                            <FontAwesomeIcon icon={faTwitter} />
                        </a>
                        <a href="https://www.facebook.com/birin243" target="_blank" rel="noopener noreferrer">
                            <FontAwesomeIcon icon={faFacebookF} />
                        </a>
                        <a href="https://www.instragram.com/birin243" target="_blank" rel="noopener noreferrer">
                            <FontAwesomeIcon icon={faInstagram} />
                        </a>
                    </div>
                </div>
            </section>
        )
    }
}
